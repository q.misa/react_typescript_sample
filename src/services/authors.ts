import { VideoAuthor } from "features/video/core/type";

export const getAuthors = (): Promise<VideoAuthor[]> => {
  return fetch(`${process.env.REACT_APP_API}/authors`).then((response) => (response.json() as unknown) as VideoAuthor[]);
};
