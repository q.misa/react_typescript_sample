import { getCategories } from './categories';
import { getAuthors } from './authors';
import {VideosApiResponse } from 'features/video/core/type';

export const getVideos = (): Promise<VideosApiResponse> => {
  return Promise
    .all([getCategories(), getAuthors()])
    .then(([categories, authors]) => {
      return {
        categories,
        authors,
      };
    });
};
