import createMuiTheme, { Theme } from '@material-ui/core/styles/createMuiTheme';
import palette from './palette';
import overrides from './overrides';


const theme: Theme = {
  ...createMuiTheme({
    props: { MuiButtonBase: { disableRipple: true } },
    palette: palette,
    overrides
  }),
};

export { theme };
export default Theme;
