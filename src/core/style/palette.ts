export default {
  primary: {
    main: '#333333',
  },
  secondary: {
    light: '#0066ff',
    main: '#0044ff',
    contrastText: '#095619',
  },
  background:{
    default:"white"
  },
  green:{
    main:"#095619"
  }
}