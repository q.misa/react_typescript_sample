import { CSSProperties } from "@material-ui/core/styles/withStyles";

export default {
  MuiCssBaseline: {
    '@global': {
      a: {
        textDecoration:"none !important",
        color:"inherit"
      },
    },
  },
  MuiAppBar:{
    root:{
      boxShadow: "none"
    }
  },
  MuiTableCell:{
    root:{
      border:"1px solid #e0e0e0"
    },
    head:{
      backgroundColor:"white",
      color:"#000",
      fontWeight:600,
      padding:"10px 16px",
      "& > span":{
        width:"100%"
      }
    }
  },
  MuiButton:{
    root:{
      boxShadow: "none !important",
      textTransform: "none",
    } as CSSProperties
  }
}