import React from 'react';
import Header from '../Header';
import { Container } from '@material-ui/core';
import useStyles from './styles';
import { ConfirmationServiceProvider } from 'components/confirmation/ConfirmationService';


export interface Props {
  children: JSX.Element[] | JSX.Element
}

const PageLayout: React.FC<Props> = ( {children}) => {
  const classes = useStyles();

  return (
    <>
      <Header />
      <ConfirmationServiceProvider>
        <Container className={classes.pageContentWrapper}>
          {children}
        </Container>
      </ConfirmationServiceProvider>
    </>
  );
}

export default PageLayout;
