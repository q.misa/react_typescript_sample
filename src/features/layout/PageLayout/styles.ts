import { makeStyles } from '@material-ui/styles';
import Theme from 'core/style/theme';

export default makeStyles((theme: Theme) => ({
  pageContentWrapper:{
    paddingTop:30
  }
}));
