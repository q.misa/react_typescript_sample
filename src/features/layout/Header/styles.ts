import { makeStyles } from '@material-ui/styles';
import Theme from 'core/style/theme';

export default makeStyles((theme: Theme) => ({
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    marginRight: '10px',
  },
  menuItemsWrapper: {
    "& a":{
      color: "gray",
      "& button":{
        color:"inherit"
      },
      "&.active":{
        color:"white"
      }
    }
  },
}));
