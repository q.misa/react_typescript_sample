import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { Link, NavLink } from 'react-router-dom';
import useStyles from './styles';
import { Button } from '@material-ui/core';

const Header: React.FC = () => {
  const classes = useStyles();

  return (
    <AppBar position='static'>
      <Toolbar>
        <Typography variant='h6' className={classes.title}>
          Videos
        </Typography>

        <div className={classes.menuItemsWrapper}>
          <NavLink to={'/'} exact>
            <Button variant={'text'}>
              Home
            </Button>
          </NavLink>

          <NavLink to={'/about'}>
            <Button variant={'text'}>
              About us
            </Button>
          </NavLink>

          <NavLink to={'/faq'}>
            <Button variant={'text'}>
              FAQ
            </Button>
          </NavLink>
        </div>

      </Toolbar>
    </AppBar>
  );
}

export default Header;
