import React, { memo } from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import PageLayout from 'features/layout/PageLayout';
import { Route, Switch } from 'components/Router';
const VideoListPage = React.lazy(() => import('features/video/pages/VideoListPage'));



export const routes = [
  {
    path: '/',
    component: VideoListPage,
  },
];

function MainRouter() {
  return (
    <Router>
      <Switch>
        <PageLayout>
          {routes.map(({ component: Component, ...rest }) => (
            <Route exact {...rest} key={rest.path}>
              <Component />
            </Route>
          ))}
        </PageLayout>
      </Switch>
    </Router>
  );
}

export default memo(MainRouter);
