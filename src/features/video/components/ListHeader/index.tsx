import React, { useContext } from 'react';
import { TableCell, TableHead } from '@material-ui/core';
import { Column, VideoRowModel } from 'features/video/core/type';
import TableRow from '@material-ui/core/TableRow';
import TableSortLabel from '@material-ui/core/TableSortLabel';
import VideoListContext from '../ListContext';


interface VideoListHeaderProps {
  columns:Column[]
}

export const VideoListHeader: React.FC<VideoListHeaderProps> = ({ columns }) => {
  const { order, orderBy, requestSort } = useContext(VideoListContext);

  const createSortHandler = (property: string) => (event: React.MouseEvent<unknown>) => {
    requestSort(property as keyof VideoRowModel );
  };

  return (
    <TableHead>
      <TableRow>
        {columns.map((column) => (
          <TableCell
            width={column.width}
            key={column.key}
            sortDirection={orderBy === column.key ? order : false}
          >
            {column.sort ? (
              <TableSortLabel
                active={orderBy === column.key}
                direction={orderBy === column.key ? order : 'asc'}
                onClick={createSortHandler(column.key)}
              >
                {column.label}
              </TableSortLabel>
            ) :  column.label}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
};


export default VideoListHeader;