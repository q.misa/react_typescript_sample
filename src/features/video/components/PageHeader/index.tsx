import React, { memo, useCallback } from 'react';
import VideoListFilter from 'features/video/components/Filter';
import { Button, Toolbar } from '@material-ui/core';
import useStyles from "./styles"


interface PageHeaderProps {
  showForm:()=>void,
  search:(searchValue:string)=> void
}

const PageHeader: React.FC<PageHeaderProps> = ({showForm, search}) => {
  const classes = useStyles();

  const handleClick = useCallback(() => {
    showForm()
  }, [])

  return (
    <Toolbar className={classes.innerHeader}>
      <VideoListFilter search={search}/>

      <Button className={classes.addBtn} variant={'contained'} onClick={handleClick}>
        Add Video
      </Button>
    </Toolbar>
  );
};

export default memo(PageHeader);