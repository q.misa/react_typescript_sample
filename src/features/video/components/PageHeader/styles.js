import makeStyles from '@material-ui/core/styles/makeStyles';

export default makeStyles(theme => ({
  innerHeader:{
    padding:0,
    display:"flex"
  },
  addBtn:{
    marginLeft:"auto",
    backgroundColor:theme.palette.success.main,
    color:"white",
    "&:hover":{
      backgroundColor:theme.palette.success.dark,
    }
  }
}))