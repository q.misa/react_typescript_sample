import { createContext } from 'react';
import { VideosDataReturnValue } from '../useVideosData';


const VideoListContext = createContext({} as VideosDataReturnValue);

export default VideoListContext;
