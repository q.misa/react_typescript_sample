import React from 'react';
import { Column, VideoRowModel } from 'features/video/core/type';
import StripedTableCell from 'components/StripedTableCell';
import StripedTableRow from 'components/StripedTableRow';

interface VideoRowProps {
  video: VideoRowModel;
  columns:Column[]
}

export const VideoRow: React.FC<VideoRowProps> = ({ video, columns }) => {
  return (
    <StripedTableRow>
      {columns.map(col => (
        <StripedTableCell key={col.key as string}>
          {col.render ? col.render(video) : video[col.key as keyof VideoRowModel]}
        </StripedTableCell>
      ))}
    </StripedTableRow>
  );
};
