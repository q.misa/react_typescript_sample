import React from 'react';
import { act, fireEvent, render, screen, waitFor } from '@testing-library/react';
import VideoRowActions from './index';
import { ThemeProvider } from '@material-ui/core/styles';
import { theme } from 'core/style/theme';
import { renderHook } from '@testing-library/react-hooks';
import { ConfirmationServiceProvider, useConfirmation } from 'components/confirmation/ConfirmationService';

const fakeVideo = {
  id: 1,
  name: 'Test',
  categoriesIds: [1, 2],
  categoriesNames: 'cate1, cat22',
  authorId: 1,
  authorName: 'author',
  formats: null,
};

describe('row actions', () => {
  const initialProps = {
    tree: {
      name: 'Tree Name',
      species_name: 'Tree Species',
      image: 'Tree Image Source',
    },
  };

  it('should contains two Edit/Delete buttons', () => {
    const { container, getByTestId } = render(
      <ThemeProvider theme={theme}>
        <VideoRowActions video={fakeVideo} />
      </ThemeProvider>,
    );

    expect(container.getElementsByTagName('button').length === 2);
  });

  test('delete action works and displays confirmation message', async () => {
    const {getByText} = render(
      <ThemeProvider theme={theme}>
        <ConfirmationServiceProvider>
          <VideoRowActions video={fakeVideo} />
        </ConfirmationServiceProvider>
      </ThemeProvider>,
    );

    const { result, waitForNextUpdate } = renderHook(() =>
      useConfirmation(),
    );
    const confirm = result.current;
    // userEvent.click(screen.getByText('Delete'));

    const deleteButton = getByText("Delete");
    fireEvent.click(deleteButton);
    // await confirm({
    //   description: 'Are you sure to delete?',
    // });
    // const labelAfterGet =

    // await waitFor(expect(confirm).toHaveBeenCalledWith({
    //   description: 'Are you sure to delete?',
    // })
    // await confirm({
    //   description: 'Are you sure to delete?',
    // })

    // mockImplementation(() => {
    //   return {
    //     fetchNumber: jest.fn(() => Promise.reject()),
    //   };
    // });



    // await act(async () => {
    //   fireEvent.click(screen.getByRole('confirmation', {name: __t('signin_or_signup')}))
    // })
    // await screen.findByAltText("error-icon")
    // await screen.findByRole('dialog');
    expect(screen.findByText("Are you sure to delete?"))//.toHaveTextContent('Are you sure to delete?');
  });

});