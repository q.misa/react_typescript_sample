import React, { useCallback, useContext } from 'react';
import { VideoRowModel } from 'features/video/core/type';
import { Button } from '@material-ui/core';
import useStyles from './styles';
import { useConfirmation } from 'components/confirmation/ConfirmationService';
import VideoListContext from '../ListContext';

interface VideoRowProps {
  video: VideoRowModel;
}

const VideoRowActions: React.FC<VideoRowProps> = ({ video }) => {
  const classes = useStyles();
  const confirm = useConfirmation();
  const { requestDelete, showForm } = useContext(VideoListContext);

  const handleDelete = (event: React.MouseEvent<unknown>) => {
    confirm({
      variant: 'danger',
      catchOnCancel: true,
      title: 'Confirmation',
      description: 'Are you sure to delete?',
    })
      .then(() => requestDelete(video.id as number))
      .catch(() => alert('No'));
  };

  const handleClickEdit = useCallback(() => {
    showForm(video)
  }, [video])

  return (
    <div className={classes.btnWrapper}>
      <Button onClick={handleClickEdit} size={'small'} variant={'contained'} className={classes.editBtn}>
        Edit
      </Button>

      <Button
        size={'small'}
        variant={'contained'}
        className={classes.deleteBtn}
        onClick={handleDelete}>
        Delete
      </Button>
    </div>
  );
}


export default VideoRowActions;