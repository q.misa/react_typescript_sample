import { makeStyles } from '@material-ui/styles';
import Theme from 'core/style/theme';

export default makeStyles((theme: Theme) => ({
  btnWrapper:{
    display:"flex",
    "& button":{
      color: "#FFFFFF",
      "&:first-child":{
        marginRight:"5px"
      }
    }
  },
  editBtn:{
    backgroundColor: theme.palette.secondary.main
  },
  deleteBtn:{
    backgroundColor: theme.palette.error.main
  }
}));
