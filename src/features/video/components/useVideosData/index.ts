import { useCallback, useEffect, useReducer } from 'react';
import { initialState, reducer, State } from 'features/video/core/reducer';
import { getVideos } from 'services/videos';
import {
  createRequest,
  createSuccess,
  deleteRequest,
  fetchDataSuccess, hideFormRequest,
  searchRequest,
  sortRequest,
  updateRequest, updateSuccess,
} from 'features/video/core/actions';
import getComparator, { Order, sort } from 'core/helper/sort';
import { VideoRowModel, VideoModel } from 'features/video/core/type';
import { REGEX_PATTERNS } from 'core/helper/regex';


export interface VideosDataReturnValue extends State {
  search: (searchValue: string) => void;
  requestSort: (property: keyof VideoRowModel) => void;
  requestDelete: (id: number) => void;
  saveVideo: (vide: VideoModel) => void;
  showForm: (vide?: VideoRowModel) => void;
  hideForm: () => void;
  videos: VideoRowModel[],
}

function useVideosData(): VideosDataReturnValue {
  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = useCallback(() => {
    getVideos()
      .then((data) => {
        dispatch(fetchDataSuccess(data));
      })
      .catch(e => {
        // dispatch({type:VideoAction.FETCH_SUCCESS, payload:"Try "})
      });
  }, [dispatch]);


  const requestSort = (str: keyof VideoRowModel) => {
    dispatch(sortRequest(str));
  };

  /**
   * call delete api and dispatch in the success
   * @param id
   */
  const requestDelete = (id: number) => {
    dispatch(deleteRequest(id));
  };

  const search = (searchText: string) => {
    if (state.searchText !== searchText) {
      dispatch(searchRequest(searchText));
    }
  };

  const getVisibleRecords = () => {
    let visibleRecords = state.data;

    if (!!state.searchText) {
      const searchText = state.searchText.toLowerCase().replace(REGEX_PATTERNS.SPECIAL_CHARACTERS, '\\$&');
      const regex = new RegExp(searchText.toLowerCase());

      visibleRecords = visibleRecords.filter(item => regex.test(item.name.toLowerCase()));
    }

    if (!!state.orderBy) {
      visibleRecords = sort(visibleRecords, getComparator(state.order, state.orderBy));
    }

    return visibleRecords;
  };

  const showForm = (video: VideoRowModel | undefined) => {
    if (video) {
      dispatch(updateRequest(video));
    }
    else {
      dispatch(createRequest());
    }
  };

  const hideForm = () => {
    dispatch(hideFormRequest());
  };

  const saveVideo = (video: VideoModel) => {
    if(video.id){
      dispatch(updateSuccess(video));
    }
    else{
      dispatch(createSuccess(video));
    }
  };

  return {
    ...state,
    videos: getVisibleRecords(),
    search,
    requestSort,
    requestDelete,
    showForm,
    hideForm,
    saveVideo,
  };
}

export default useVideosData;
