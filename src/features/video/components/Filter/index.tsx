import React, { useContext, useState } from 'react';
import { Button, TextField } from '@material-ui/core';
import useStyles from './styles';


export interface FilterProps {
  search: (searchValue: string) => void;
}

const VideoListFilter: React.FC<FilterProps> = ({ search }) => {
  const classes = useStyles();
  const [value, setValue] = useState('');

  const handleSearch = (event: React.MouseEvent<HTMLButtonElement>): void => {
    search(value);
  };

  const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>): void => {
    if (event.key === 'Enter') {
      search(value);
    }
  };

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>): void => {
    const newValue = event.target.value;
    setValue(newValue);
    if(newValue === ""){
      search("");
    }
  };

  const clearSearch = (event: React.MouseEvent<HTMLImageElement>): void => {
    setValue("");
    search("");
  };

  return (
    <div>
      <TextField
        type={'text'}
        aria-label={'search video by name'}
        placeholder={'Search…'}
        onChange={handleChange}
        className={classes.searchInput}
        variant={'outlined'}
        value={value}
        onKeyDown={handleKeyDown}
        InputProps={{
          endAdornment: (
            <Button variant={'contained'} onClick={handleSearch} aria-label={'Search'}>
              Search
            </Button>
          ),
          startAdornment: !!value ? (
            <img
              src={'/images/close.svg'}
              onClick={clearSearch}
              className={classes.clearSearch}
            />
          ) : null,
        }}
      />
    </div>
  );
};

export default VideoListFilter;