import { makeStyles } from '@material-ui/styles';
import Theme from 'core/style/theme';

export default makeStyles((theme: Theme) => ({
  searchInput: {
    '& > div': {
      padding: 0,
      position:"relative"
    },
    '& input': {
      padding: '0 5px 0 25px',
    },
    '& button': {
      backgroundColor: theme.palette.info.main,
      borderRadius: '0 4px 4px 0',
      color: 'white',
    },
  },
  clearSearch: {
    width: '13px',
    position: 'absolute',
    cursor: 'pointer',
    left:"3px"
  },
}))
;
