import React, { useMemo } from 'react';
import { Button, Paper, Table, TableBody, TableContainer } from '@material-ui/core';
import { Column, VideoRowModel } from 'features/video/core/type';
import VideoRowActions from '../Actions';
import { VideoRow } from '../Row';
import VideoListHeader from '../ListHeader';

interface VideosTableProps {
  videos: VideoRowModel[];
}

export const VideoList: React.FC<VideosTableProps> = ({ videos }) => {
  const columns: Column[] = useMemo(() =>
    [{
      key: 'name',
      label: 'Video Name',
      sort: true,
      align: 'left',
    }, {
      key: 'authorName',
      label: 'Author',
      sort: true,
      align: 'justify',
    }, {
      key: 'categoriesNames',
      label: 'Categories',
      sort: true,
      align: 'justify',
    },{
      key: 'releaseDate',
      label: 'Release Date',
      sort: true,
      align: 'justify',
      render: (video) => {
        return "-"
      }
    }, {
      key: 'format',
      sort: false,
      label: 'Highest quality format',
      align: 'justify',
      render: (video) => {
        const { formats } = video;
        if (formats) {
          const msxResolution = Math.max.apply(Math, Object.values(formats).map(item => parseInt(item.res))) + "p"
          const maxSize = Math.max.apply(Math, Object.values(formats).map(item => item.size))
          return `${msxResolution} ${maxSize}`;
        }
        else{
          return ""
        }
      },
    },{
      key: 'actions',
      label: 'Options',
      align: 'justify',
      sort: false,
      width: 150,
      render: (video) => <VideoRowActions video={video} />,
    }], []);

  return (
    <TableContainer component={Paper} style={{ marginTop: '40px' }}>
      <Table>
        <VideoListHeader columns={columns} />

        <TableBody>
          {videos.map((video) => (
            <VideoRow
              key={video.id}
              video={video}
              columns={columns}
            />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};
