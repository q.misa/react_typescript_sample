import { render } from '@testing-library/react';
import { ThemeProvider } from '@material-ui/core/styles';
import { theme } from 'core/style/theme';
import React from 'react';
import { VideoList } from './index';


const fakeVideoList = [{
  id: 1,
  name: 'Test',
  categoriesIds: [1, 2],
  categoriesNames: 'cate1, cat22',
  authorId: 1,
  authorName: 'author',
  formats: null,
  releaseDate: null
}];

const fakeColumns = [{
  key: 'column1',
  label: 'Column1',
  align: 'justify',
}]


describe('videos list', () => {

  it('should contains correct cells', () => {
    const { container, getByTestId } = render(
      <ThemeProvider theme={theme}>
        <VideoList videos={fakeVideoList}/>
      </ThemeProvider>,
    );

    expect(container.getElementsByClassName('MuiTableCell-root').length === fakeVideoList.length * fakeColumns.length);
  });
})