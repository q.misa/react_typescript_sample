import React  from 'react';
import VideoForm from './index';
import { render, fireEvent, cleanup, act } from '@testing-library/react';
import { ThemeProvider } from '@material-ui/core/styles';
import { theme } from 'core/style/theme';
import VideoListContext from '../ListContext';
import { initialState } from 'features/video/core/reducer';
import { renderHook } from '@testing-library/react-hooks';
import { useForm } from 'react-hook-form';


afterEach(cleanup);

const contextValue = {
  ...initialState,
  videos: [],
  editingVideo: {
    name: '',
    authorId: null,
    categoriesIds: [],
    authorName: '',
    categoriesNames: '',
    id: null,
    formats: null,
  },
  search: jest.fn(),
  requestSort: jest.fn(),
  requestDelete: jest.fn(),
  saveVideo: jest.fn(),
  hideForm: jest.fn(),
  showForm: jest.fn(),
  authorsById: { 1: { id: 1, name: 'Tom Hanks' } },
  categoriesById: { 1: { name: 'Drama', id: 1 } },
};

const mockValues = {
  name: 'Forrest Gump',
  authorId: 1,
  categoriesIds: [1],
};

const setup = () => {
  const { container, getByTestId, getByText } = render(
    <ThemeProvider theme={theme}>
      <VideoListContext.Provider value={contextValue}>
        <VideoForm />
      </VideoListContext.Provider>
    </ThemeProvider>,
  );

  return {
    container, getByTestId, getByText
  };
};

test('should watch inputs correctly', () => {
  const { getByTestId } = setup();
  renderHook(() =>
    useForm({
      defaultValues: {
        name: '',
        authorId: null,
        categoriesIds: [],
      },
    }),
  );

  const nameInput = getByTestId('name') as HTMLInputElement;
  const authorIdInput = getByTestId('authorId') as HTMLSelectElement;
  const categoriesIdsInput = getByTestId('categoriesIds') as HTMLSelectElement;

  fireEvent.input(nameInput, {
    target: {
      value: mockValues.name,
    },
  });

  fireEvent.input(authorIdInput, {
    target: {
      value: mockValues.authorId,
    },
  });

  fireEvent.select(categoriesIdsInput, {
    target: {
      value: [1],
    },
  });

  expect(nameInput.value).toEqual(mockValues.name);
  expect(authorIdInput.value).toBe(mockValues.authorId.toString());
  expect(categoriesIdsInput.value.length).toBe(mockValues.categoriesIds.length);
  expect(categoriesIdsInput.value[0]).toBe('1');

});


test('should display correct error message', async () => {
  const { getByTestId, getByText, container } = setup();
  renderHook(() =>
    useForm({
      defaultValues: {
        name: '',
        authorId: null,
        categoriesIds: [],
      },
    }),
  );

  const nameInput = getByTestId('name');
  const authorIdInput = getByTestId('authorId');
  const categoriesIdsInput = getByTestId('categoriesIds');
  const submitButton = getByText("Save")

  await act(async () => {
    fireEvent.submit(submitButton);
  });

  expect(getByText("Name is required!")).toBeTruthy()
  expect(getByText("Author is required!")).toBeTruthy()
  expect(getByText("You must choose one Category at least!")).toBeTruthy()
});

