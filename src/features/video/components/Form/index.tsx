import React, { useCallback, useContext } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from '@material-ui/core/DialogTitle/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import { useForm, Controller } from 'react-hook-form';
import useStyles from './styles';
import { Button, DialogActions, FormLabel, MenuItem, TextField } from '@material-ui/core';
import VideoListContext from '../ListContext';


export const VideoForm: React.FC = () => {
  const { authorsById, categoriesById, saveVideo, editingVideo, hideForm } = useContext(VideoListContext);
  const classes = useStyles();
  const { register, control, handleSubmit, errors, getValues } = useForm({
    defaultValues: {
      name: editingVideo?.name ?? '',
      authorId: editingVideo?.authorId ?? null,
      categoriesIds: editingVideo?.categoriesIds ?? [],
    },
  });

  const { name, authorId = null, categoriesIds = [] } = getValues();

  const submit = (values: any) => {
    saveVideo({
      id: editingVideo?.id,
      ...values,
    });
  };

  const closeDialog = useCallback(() => {
    hideForm();
  }, []);

  return (
    <Dialog
      onClose={hideForm}
      fullWidth
      maxWidth={'sm'}
      aria-labelledby='video form dialog'
      open={true}>
      <DialogTitle onClick={hideForm}>
        {editingVideo ? `${editingVideo.name}` : 'New Video'}
      </DialogTitle>
      <DialogContent dividers>
        <form onSubmit={handleSubmit(submit)} noValidate>
          <div className={classes.formItem}>
            <FormLabel>Video Name</FormLabel>
            <TextField
              variant='outlined'
              name={'name'}
              size={'small'}
              value={name}
              inputRef={register({
                required: 'Name is required!',
              })}
              inputProps={{
                'data-testid':"name"
              }}
              style={{ width: '100%' }}
              helperText={errors.name?.message ?? ''}
              error={errors.name ? true : false}
              type={'text'}
              placeholder={'Video Name'}
            />
          </div>

          <div className={classes.formItem}>
            <FormLabel>Video Author</FormLabel>
            <Controller
              name={'authorId'}
              control={control}
              inputRef={register}
              rules={{
                required: 'Author is required!',
              }}
              as={
                <TextField
                  select
                  variant='outlined'
                  name={'authorId'}
                  size={'small'}
                  inputProps={{
                    'data-testid':"authorId"
                  }}
                  value={authorId}
                  style={{ width: '100%' }}
                  helperText={errors.authorId?.message}
                  error={errors.authorId ? true : false}
                >
                  {Object.values(authorsById).map(({ id, name }) => (
                    <MenuItem key={id} value={id}>{name}</MenuItem>
                  ))}
                </TextField>
              }
            />
          </div>

          <div className={classes.formItem}>
            <FormLabel>Video category</FormLabel>
            <Controller
              name={'categoriesIds'}
              control={control}
              inputRef={register}
              rules={{
                validate: value => {
                  if (value.length === 0) {
                    return 'You must choose one Category at least!';
                  }
                },
              }}
              as={
                <TextField
                  select
                  variant='outlined'
                  name={'categoriesIds'}
                  inputProps={{
                    multiple: true,
                    'data-testid':"categoriesIds"
                  }}
                  defaultValue={categoriesIds}
                  style={{ width: '100%' }}
                  error={errors?.categoriesIds ? true : false}
                  helperText={(errors.categoriesIds as any)?.message}
                >
                  {Object.values(categoriesById).map(({ id, name }) => (
                    <MenuItem key={id} value={id}>{name}</MenuItem>
                  ))}
                </TextField>
              }
            />
          </div>

          <DialogActions>
            <Button
              variant={'contained'}
              className={classes.submitBtn}
              color={'secondary'}
              type={'submit'}>
              Save
            </Button>

            <Button
              variant={'outlined'}
              color={'primary'}
              onClick={closeDialog}>
              Cancel
            </Button>

          </DialogActions>

        </form>
      </DialogContent>
    </Dialog>
  );
};

export default VideoForm;
