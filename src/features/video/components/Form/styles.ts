import { makeStyles } from '@material-ui/styles';
import Theme from 'core/style/theme';

export default makeStyles((theme: Theme) => ({
  toggleModalBtnWrapper: {
    marginLeft:'auto',
    "& button":{
      backgroundColor: theme.palette.success.main,
      color:"white"
    }
  },
  formItem:{
    display:"flex",
    margin: "10px",
    alignItems:"center",
    "& label":{
      width:"150px"
    }
  },
  submitBtn:{
    color:"white"
  }
}))
;
