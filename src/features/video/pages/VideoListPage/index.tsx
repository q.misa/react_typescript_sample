import useVideosData from 'features/video/components/useVideosData';
import React, { memo } from 'react';
import Typography from '@material-ui/core/Typography';
import VideoListContext from 'features/video/components/ListContext';
import { VideoList } from 'features/video/components/List';
import PageHeader from 'features/video/components/PageHeader';
import VideoForm from 'features/video/components/Form';


const VideoListPage: React.FC = () => {
  const {videos, search, showForm, visibleForm, ...rest} = useVideosData();

  return (
    <VideoListContext.Provider value={{videos, search, showForm, visibleForm, ...rest}}>
      <>
        <Typography gutterBottom variant={'h4'} component={'h1'}>VManager Demo v0.0.1</Typography>

        <PageHeader search={search} showForm={showForm}/>

        <VideoList videos={videos} />
        
        {visibleForm && <VideoForm/>}
      </>
    </VideoListContext.Provider>
  );
};

export default memo(VideoListPage);