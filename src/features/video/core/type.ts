export interface Category {
  id: number;
  name: string;
}

export interface CategoriesById {
  [key: number]: Category
}

export interface AuthorsById {
  [key: number]: Author
}

export interface Video {
  id: number;
  catIds: number[];
  name: string;
}

export interface Author {
  id: number;
  name: string;
}

export interface VideoAuthor extends Author {
  videos: Video[];
}

export interface VideosApiResponse {
  categories: Category[],
  authors: VideoAuthor[]
}

export interface VideoModel {
  id: number | null;
  name: string;
  authorId: number | null;
  categoriesIds: number[];
}

export interface Format {
  res: string;
  size: number;
}

export interface VideoRowModel extends VideoModel {
  authorName: string,
  categoriesNames: string;
  releaseDate:string | null,
  formats: {
    [key: string]: Format
  } | null;
}


export interface Column {
  key: string;
  label: string;
  align: 'left' | 'right' | 'justify',
  width?: number,
  sort: boolean,
  render?: (record: VideoRowModel) => JSX.Element | string;
}