import { VideoRowModel, VideoModel, VideosApiResponse } from './type';

export enum VideoAction {
  FETCH_SUCCESS = "@@videos/FETCH_SUCCESS",
  FETCH_ERROR = "@@videos/FETCH_ERROR",
  SORT_REQUEST = "@@videos/SORT_REQUEST",
  SEARCH_REQUEST = "@@videos/SEARCH_REQUEST",
  DELETE_REQUEST = "@@videos/DELETE_REQUEST",
  CREATE_REQUEST = "@@videos/CREATE_REQUEST",
  CREATE_SUCCESS = "@@videos/CREATE_SUCCESS",
  UPDATE_REQUEST = "@@videos/UPDATE_REQUEST",
  UPDATE_SUCCESS = "@@videos/UPDATE_SUCCESS",
  HIDE_FORM_REQUEST = "@@videos/HIDE_FORM_REQUEST"
}

export type Action =
  | { type: VideoAction.FETCH_SUCCESS; payload: VideosApiResponse }
  | { type: VideoAction.FETCH_ERROR; payload: string }
  | { type: VideoAction.SORT_REQUEST; payload: keyof VideoRowModel}
  | { type: VideoAction.DELETE_REQUEST; payload: number}
  | { type: VideoAction.SEARCH_REQUEST; payload: string}
  | { type: VideoAction.CREATE_REQUEST;}
  | { type: VideoAction.CREATE_SUCCESS; payload: VideoModel}
  | { type: VideoAction.UPDATE_REQUEST; payload: VideoRowModel}
  | { type: VideoAction.UPDATE_SUCCESS; payload: VideoModel}
  | { type: VideoAction.HIDE_FORM_REQUEST;}


export function fetchDataSuccess(payload: VideosApiResponse): Action {
  return { type: VideoAction.FETCH_SUCCESS, payload };
}

export function fetchDataError(payload: string): Action {
  return { type: VideoAction.FETCH_ERROR, payload };
}

export function sortRequest(payload: keyof VideoRowModel): Action {
  return { type: VideoAction.SORT_REQUEST, payload };
}

export function searchRequest(payload: string): Action {
  return { type: VideoAction.SEARCH_REQUEST, payload };
}

export function deleteRequest(payload: number): Action {
  return { type: VideoAction.DELETE_REQUEST, payload };
}

export function createRequest(): Action {
  return { type: VideoAction.CREATE_REQUEST};
}

export function createSuccess(payload: VideoModel): Action {
  return { type: VideoAction.CREATE_SUCCESS, payload };
}

export function updateRequest(payload: VideoRowModel): Action {
  return { type: VideoAction.UPDATE_REQUEST, payload};
}

export function updateSuccess(payload: VideoModel): Action {
  return { type: VideoAction.UPDATE_SUCCESS, payload };
}

export function hideFormRequest(): Action {
  return { type: VideoAction.HIDE_FORM_REQUEST };
}

