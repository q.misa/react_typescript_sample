import { Action, VideoAction } from './actions';
import { AuthorsById, CategoriesById, VideoAuthor, VideoRowModel } from './type';
import { Order } from 'core/helper/sort';
import generateId from 'core/helper/generateId';

export interface State {
  loading: boolean;
  data: VideoRowModel[];
  error: string | null,
  searchText: string;
  order: Order;
  orderBy: keyof VideoRowModel | null;
  categoriesById: CategoriesById,
  authorsById: AuthorsById,
  visibleForm: boolean,
  editingVideo: VideoRowModel | null
}

export const initialState: State = {
  loading: false,
  data: [],
  searchText: '',
  error: null,
  order: 'asc',
  orderBy: null,
  categoriesById: {},
  authorsById: [],
  visibleForm: false,
  editingVideo: null,
};

export function reducer(state: State, action: Action): State {
  switch (action.type) {
    case VideoAction.FETCH_SUCCESS:
      const { categories, authors } = action.payload;
      let categoriesById: CategoriesById = [];
      let authorsById: AuthorsById = [];
      let videos: VideoRowModel[] = [];

      categories.reduce((accu, item) => {
        accu[item.id] = item;
        return accu;
      }, categoriesById);

      authors.reduce((accu, item) => {
        accu[item.id] = item;
        return accu;
      }, authorsById);

      return {
        ...state,
        loading: false,
        categoriesById,
        authorsById,
        data: authors.reduce((accu, item: VideoAuthor) => {
          const { videos = [], ...author } = item;
          videos.forEach(({ catIds = [], ...video }) => {
            accu.push({
              ...video,
              authorId: author.id,
              authorName: author.name,
              categoriesNames: catIds.map(id => categoriesById[id]['name']).join(', '),
              categoriesIds: catIds,
              formats: null,
              releaseDate: null
            });
          });

          return accu;
        }, videos),
      };

    case VideoAction.SORT_REQUEST:
      const orderBy = action.payload;
      const isAsc = orderBy === state.orderBy && state.order === 'asc';
      const order = isAsc ? 'desc' : 'asc';
      return {
        ...state,
        orderBy,
        order,
      };


    case VideoAction.DELETE_REQUEST:
      return {
        ...state,
        data: state.data.filter(item => item.id !== action.payload),
      };

    case VideoAction.SEARCH_REQUEST:
      return {
        ...state,
        searchText: action.payload,
      };

    case VideoAction.CREATE_REQUEST: {
      return {
        ...state, visibleForm: true,
      };
    }

    case VideoAction.HIDE_FORM_REQUEST: {
      return {
        ...state, visibleForm: false, editingVideo: null,
      };
    }

    case VideoAction.CREATE_SUCCESS: {
      const { name, authorId, categoriesIds } = action.payload;

      return {
        ...state,
        visibleForm: false,
        data: [...state.data, {
          id: generateId(),
          name,
          authorId,
          authorName: state.authorsById[authorId!]['name'],
          categoriesIds,
          categoriesNames: categoriesIds.map(id => state.categoriesById[id]['name']).join(', '),
          formats: {
            one: { res: "6080p", size: 1000 },
          },
          releaseDate: null
        }],
      };
    }

    case VideoAction.UPDATE_REQUEST: {
      return {
        ...state,
        visibleForm: true,
        editingVideo: action.payload,
      };
    }

    case VideoAction.UPDATE_SUCCESS: {
      const { name, authorId, categoriesIds, id } = action.payload;

      return {
        ...state,
        visibleForm: false,
        data: state.data.map(item => item.id === id ? ({
          ...item,
          name,
          authorId,
          authorName: state.authorsById[authorId!]['name'],
          categoriesIds,
          categoriesNames: categoriesIds.map(id => state.categoriesById[id]['name']).join(', '),
        }) : item),
      };
    }

    default:
      throw new Error();
  }
}
