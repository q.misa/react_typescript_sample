import React from 'react';
import MainRouter from './features/layout/MainRouter';
import { StylesProvider, jssPreset, ThemeProvider } from '@material-ui/styles';
import jssExtend from 'jss-plugin-extend';
import { create } from 'jss';
import { theme } from 'core/style/theme';
import CssBaseline from '@material-ui/core/CssBaseline';


const jss = create({
  ...jssPreset(),
  plugins: [jssExtend(), ...jssPreset().plugins],
  // We define a custom insertion point that JSS will look for injecting the styles in the DOM.
  insertionPoint: document.getElementById('jss-insertion-point') ?? undefined,
});

const App: React.FC = () => {
  return (
    <StylesProvider jss={jss}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <MainRouter />
      </ThemeProvider>
    </StylesProvider>
  );
};


export default App;
