import React, { memo, Suspense } from 'react';
import { Switch as RSwitch, SwitchProps, Redirect } from 'react-router-dom';
import WaitLayout from 'components/WaitLayout';

interface Props extends SwitchProps {
  fallback?: React.ReactNode;
  includeNotFound?: boolean;
}

function Switch({ fallback, children, includeNotFound = true, ...others }: Props) {
  return (
    <Suspense fallback={fallback ?? <WaitLayout />}>
      <RSwitch {...others}>
        {children}
        {includeNotFound && <Redirect from="*" to="/404" />}
      </RSwitch>
    </Suspense>
  );
}

export default memo(Switch);
