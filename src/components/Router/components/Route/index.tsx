import React, { memo } from 'react';
import { Route as RRoute, RouteProps, Redirect, useLocation } from 'react-router-dom';

interface Props extends RouteProps {
  authorize?: boolean;
  title?: string;
}

function Route({ authorize = false, title, children, ...others }: Props) {
  const isAuthorized = true;
  const { pathname } = useLocation();

  if (authorize && !isAuthorized) {
    return (
      <Redirect
        to={{
          pathname: '/login',
          state: { from: pathname },
        }}
      />
    );
  }
  return <RRoute {...others}>{children}</RRoute>;
}

export default memo(Route);
