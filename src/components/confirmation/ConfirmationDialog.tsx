import * as React from 'react';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Button,
} from '@material-ui/core';

export interface ConfirmationOptions {
  catchOnCancel?: boolean;
  variant?: 'danger' | 'info';
  title?: string;
  description?: string;
}

interface ConfirmationDialogProps extends ConfirmationOptions {
  open: boolean;
  onSubmit: () => void;
  onClose: () => void;
}

export const ConfirmationDialog: React.FC<ConfirmationDialogProps> = (props) => {
  const {
    open,
    title="",
    variant="danger",
    description="",
    onSubmit,
    onClose,
  } = props;

  return (
    <Dialog open={open} role={'dialog'}>
      <DialogTitle id='alert-dialog-title'>{title}</DialogTitle>
      <DialogContent>
        <DialogContentText>{description}</DialogContentText>
      </DialogContent>
      <DialogActions>
        {variant === 'danger' && (
          <>
            <Button color='secondary' onClick={onSubmit}>
              Yes
            </Button>
            <Button color='primary' onClick={onClose} autoFocus>
              Cancel
            </Button>
          </>
        )}

        {variant === 'info' && (
          <Button color='primary' onClick={onSubmit}>
            OK
          </Button>
        )}
      </DialogActions>
    </Dialog>
  );
};
