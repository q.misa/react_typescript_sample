import React, { memo } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import useStyles from './styles';

interface Props {
  hasBackground?: boolean;
}
function WaitLayout({ hasBackground = false }: Props) {
  const classes = useStyles({ hasBackground });
  return (
    <div className={classes.root}>
      <CircularProgress />
    </div>
  );
}

export default memo(WaitLayout);
